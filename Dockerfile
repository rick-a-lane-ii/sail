FROM openjdk:8-jre-alpine

ENV BASE /home

WORKDIR ${BASE}
ADD README.md $BASE
